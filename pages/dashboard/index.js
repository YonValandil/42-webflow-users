import React, { useState, useEffect } from 'react'
import { useRouter } from 'next/router'
import axios from 'axios'
import atob from 'atob'

import { makeStyles } from '@material-ui/core/styles'
import Container from '@material-ui/core/Container'
import Box from '@material-ui/core/Box'
import {
  FormControl,
  FormHelperText,
  Input,
  InputLabel,
  CircularProgress
} from '@material-ui/core'
import Button from '@material-ui/core/Button'

const useStyles = makeStyles(() => ({
  /* stylelint-disable */
  container: {
    margin: '10px',
    alignItems: 'center',
    justifyContent: 'center'
  },
  title: {
    textAlign: 'center',
    marginBottom: '20px'
  },
  formquery: {
    display: 'flex',
    justifyContent: 'space-around'
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
    marginBottom: '50px'
  },
  formElement: {
    textAlign: 'center',
    marginBottom: '10px'
  },
  button: {
    margin: 'auto'
  },
  loader: {
    display: 'block',
    marginTop: '20px',
    marginLeft: '60px'
  },
  loaderCreate: {
    display: 'block',
    margin: 'auto',
    marginBottom: '40px'
  }
  /* stylelint-enable */
}))

const Dashboard = () => {
  const classes = useStyles()
  const router = useRouter()
  const [loadingCreate, setLoadingCreate] = useState(false)
  const [loading, setLoading] = useState(false)
  const [clickjacked, setclickjacked] = useState(false)
  const [data, setData] = useState(false)
  const [notif, setNotif] = useState(false)
  const [query, setQuery] = useState({
    campus_id: '',
    email: '',
    first_name: '',
    last_name: '',
    password: ''
  })

  useEffect(async () => {
    if (router.query.state === undefined) {
      return
    }

    if (window.localStorage.getItem('oauth-state') !== decodeURIComponent(atob(router.query.state))) {
      setData(false)
      setclickjacked(true)
      return console.log('You may have been click-jacked!')
    }
  })

  // Update inputs value
  const handleParam = () => (e) => {
    const name = e.target.name
    const value = e.target.value
    setQuery((prevState) => ({
      ...prevState,
      [name]: value
    }))
  }

  const handleSubmit = async (e) => {
    e.preventDefault()
    setLoadingCreate(true)

    if (clickjacked) {
      setLoading(false)
      return
    }

    const res = await axios.get('../api/createUser', {
      params: { query }
    }).catch(err => console.error(err))
    setLoadingCreate(false)
    if (res && res.status === 200) {
      setNotif(true)
    }
  }

  const handleUsers = async (e) => {
    e.preventDefault()
    setLoading(true)

    if (clickjacked) {
      setLoading(false)
      return
    }

    const res = await axios.get('../api/getUsers')
      .catch(err => console.error(err))
    setLoading(false)
    if (res && res.status === 200) {
      setData(res.data)
    }
  }

  return (
    <>
      <Container maxWidth='sm'>
        <Box my={4}>
          <h1 className={classes.title}>Dashboard</h1>

          <p>{notif && 'User has been created !'}</p>

          <h3 className={classes.title}>Create user</h3>

          <form className={classes.form}>
            <FormControl>
              <InputLabel>Campus id</InputLabel>
              <Input
                type='text'
                name='campus_id'
                required
                placeholder='Campus id'
                className='form-control'
                value={query.campus_id}
                onChange={handleParam()}
              />
            </FormControl>

            <FormControl>
              <InputLabel htmlFor='email'>Email</InputLabel>
              <Input
                type='text'
                name='email'
                required
                placeholder='email id'
                className='form-control'
                value={query.email}
                onChange={handleParam()}
              />
            </FormControl>

            <FormControl>
              <InputLabel htmlFor='first_name'>First name</InputLabel>
              <Input
                type='text'
                name='first_name'
                required
                placeholder='first_name id'
                className='form-control'
                value={query.first_name}
                onChange={handleParam()}
              />
            </FormControl>

            <FormControl>
              <InputLabel htmlFor='last_name'>Last name</InputLabel>
              <Input
                type='text'
                name='last_name'
                required
                placeholder='last_name id'
                className='form-control'
                value={query.last_name}
                onChange={handleParam()}
              />
            </FormControl>

            <FormControl>
              <InputLabel htmlFor='password'>Password</InputLabel>
              <Input
                htmlFor='password'
                type='text'
                name='password'
                required
                placeholder='password'
                className='form-control'
                value={query.password}
                onChange={handleParam()}
              />
              <FormHelperText id='my-helper-text'>Have to -----</FormHelperText>
            </FormControl>

            <FormControl>
              <Button className={classes.button} type='button' variant='contained' color='secondary' onClick={handleSubmit}>
                Create user
              </Button>
            </FormControl>
          </form>

          {loadingCreate && <CircularProgress className={classes.loaderCreate} />}
          {!loading && clickjacked && 'You may have been click-jacked!'}

          <h3 className={classes.title}>Test list users</h3>
          <Button type='button' variant='contained' color='secondary' onClick={handleUsers}>
            Send request list user
          </Button>
          {loading && <CircularProgress className={classes.loader} />}
          {!loading && data &&
            data.map((el, datakey) => (
              <p key={datakey}>
                {Object.values(el).map((value, key) => (
                  <span key={key}>
                    <strong>{Object.keys(el).filter((v, k) => { return k === key })} : </strong>{' '}
                    {value}
                    <br />
                  </span>
                ))}
              </p>
            ))}
        </Box>
      </Container>
    </>
  )
}

export default Dashboard
