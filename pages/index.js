import React, { useState, useEffect } from 'react'
import Head from 'next/head'
import { makeStyles } from '@material-ui/core/styles'
import { useRouter } from 'next/router'
import btoa from 'btoa'
import styles from '../styles/Home.module.css'
import Button from '@material-ui/core/Button'
import { CircularProgress } from '@material-ui/core'

const useStyles = makeStyles(() => ({
  loader: {
    margin: 'auto',
    marginTop: '20px'
  }
}))

export default function Home ({ state }) {
  const classes = useStyles()
  const [loading, setLoading] = useState(true)

  const router = useRouter()
  const encodeState = encodeURIComponent(btoa(state))
  const url = `https://api.intra.42.fr/oauth/authorize?client_id=b6107c8439b4a48b023c3385a19caf800e1e6d884000a683ffe45b6dc937d4e7&redirect_uri=http%3A%2F%2F127.0.0.1%3A3000%2Fdashboard&response_type=code&state=${encodeState}`

  useEffect(async () => {
    window.localStorage.setItem('oauth-state', state)
    setTimeout(() => {
      router.push(url)
      setLoading(false)
    }, 3000)
  }, [])

  return (
    <div className={styles.container}>
      <Head>
        <title>42 Webflow</title>
        <meta name='description' content='42 Webflow user using Oauth2' />
        <link rel='icon' href='/favicon.ico' />
      </Head>

      <main className={styles.main}>
        <h1>
          42 Webflow
        </h1>

        <p>You'll be redirected to the 42's authentication API</p>
        {loading && <CircularProgress className={classes.loader} />}
        <Button variant='contained' color='secondary'>
          <a
            href={url}
          >
            Go to the Authorization API manually
          </a>
        </Button>

      </main>

      <footer className={styles.footer}>
        <p>footer</p>
      </footer>
    </div>
  )
}

export const getStaticProps = async () => {
  const generateRandomString = () => {
    let randomString = ''
    const randomNumber = Math.floor(Math.random() * 10)

    for (let i = 0; i < 20 + randomNumber; i++) {
      randomString += String.fromCharCode(33 + Math.floor(Math.random() * 94))
    }

    return randomString
  }
  const randomString = generateRandomString()
  const state = randomString

  return {
    props: {
      state
    }
  }
}
