
import React from 'react'
import Head from 'next/head'
import '../styles/globals.css'
import { CssBaseline, Paper } from '@material-ui/core'
import { ThemeProvider, makeStyles } from '@material-ui/core/styles'
import theme from '../utils/theme'

const useStyles = makeStyles({
  root: {
    flexGrow: 1,
    overflow: 'hidden',
    minHeight: '100vh'
  }
})

function MyApp (props) {
  const { Component, pageProps } = props
  const classes = useStyles()

  React.useEffect(() => {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector('#jss-server-side')
    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles)
    }
  }, [])

  return (
    <>
      <Head>
        <title>My page</title>
        <meta name='viewport' content='minimum-scale=1, initial-scale=1, width=device-width' />
      </Head>
      <ThemeProvider theme={theme}>
        {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
        <CssBaseline />
        <Paper className={classes.root}>
          <Component {...pageProps} />
        </Paper>
      </ThemeProvider>
    </>
  )
}

export default MyApp
