import config from '../../utils/config.js'
import { connectoauth, createUser } from '../../utils/ftapi/ftapi.js'

export default async (req, res) => {
  const query = JSON.parse(req.query.query)

  const dataUser = {
    campusId: '1',
    email: query.email,
    first_name: query.first_name,
    last_name: query.last_name,
    password: query.password
  }

  // redirectUri is optional because it's LOCALHOST by default inside the config.js of ftapi
  const client = await connectoauth(req, config.data.client_id, config.data.client_secret)

  const newUser = await createUser(
    client,
    dataUser.campusId,
    dataUser.email,
    dataUser.first_name,
    dataUser.last_name,
    dataUser.password
  ).catch(err => console.error(err))
  console.log('createUser status => ', newUser.status)
  console.log('req => ', newUser.data)

  res.status(200).json(newUser.data)
}
