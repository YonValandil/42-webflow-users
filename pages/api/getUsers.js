import config from '../../utils/config.js'
import { connectoauth } from '../../utils/ftapi/ftapi.js'

export default async (req, res) => {
  const client = await connectoauth(req, config.data.client_id, config.data.client_secret, config.redirectUri)

  const user = await client.get('/v2/cursus/42/users', {
    params: { per_page: 4 }
  }).catch(err => console.error(err))

  res.status(200).json(user.data)
}
