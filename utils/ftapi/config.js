export default {
  baseurl: 'https://api.intra.42.fr',
  redirectUri: 'http://127.0.0.1:3000/dashboard',
  data: {
    grant_type: 'client_credentials',
    client_id: '',
    client_secret: ''
  }
}
