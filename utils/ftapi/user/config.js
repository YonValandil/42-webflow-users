export default {
  user: {
    campus_id: '1',
    cursus_users_attributes: [{
      begin_at: '2022-06-01 13:44:19 UTC',
      cursus_id: '1',
      end_at: '2070-03-01 13:43:10 UTC',
      user_id: ''
    }],
    email: 'test@email.com',
    first_name: 'ftapi',
    kind: 'student',
    languages_users_attributes: [{
      language_id: '1',
      position: '1',
      user_id: ''
    }],
    last_name: 'bot',
    password: '@FTAPItotoro1234@',
    pool_month: 'may',
    pool_year: '2017',
    status: 'admis',
    user_candidature_attributes: {
      birth_city: 'Bangkok',
      birth_country: 'Japan',
      birth_date: '1998-06-08',
      contact_affiliation: 'parent',
      contact_first_name: 'Dewayne',
      contact_last_name: 'Nader',
      contact_phone1: '0695847362',
      contact_phone2: '0491314200',
      country: 'Turks and Caicos Islands',
      gender: 'male',
      language: '',
      max_level_logic: '2',
      max_level_memory: '1',
      meeting_date: '2017-10-05',
      other_information: '3',
      pin: '4242',
      piscine_date: '2017-11-15',
      postal_city: 'Emelyborough',
      postal_complement: '',
      postal_country: 'Lesotho',
      postal_street: '621 Tromp Lakes',
      postal_zip_code: '10047',
      user_id: '',
      zip_code: '89457-3613'
    }
  }
}
