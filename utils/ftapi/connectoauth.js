import axios from 'axios'
import config from './config.js'
import oauth from 'axios-oauth-client'

let SINGLETON = 0

const client = axios.create({
  baseURL: config.baseurl
})

const getToken = async (client, data) => {
  try {
    const config = {
      headers: {
        'content-type': 'application/x-www-form-urlencoded'
      }
    }

    const getOwnerCredentials = oauth.client(axios.create(), {
      url: data.url,
      grant_type: data.grant_type,
      client_id: data.client_id,
      client_secret: data.client_secret,
      redirect_uri: data.redirectUri,
      code: data.code,
      scope: data.scope
      // state: data.state
    }, config)
    const auth = await getOwnerCredentials()
    client.defaults.headers.common.Authorization = 'Bearer ' + auth.access_token
    return auth
  } catch (error) {
    console.error(error)
  }
}

const interceptor = async (client, data) => {
  client.interceptors.response.use(
    success => success,
    async error => {
      const originalRequest = error.config
      if (
        error.response.status === 401 &&
          originalRequest.url.includes('/oauth/token')
      ) {
        return Promise.reject(error)
      } else if (error.response.status === 401 && !originalRequest._retry) {
        originalRequest._retry = true
        const token = await getToken(client, data)
        originalRequest.headers.Authorization = 'Bearer ' + token.access_token
        return client(originalRequest)
      }
      return Promise.reject(error)
    }
  )
}

export const connectoauth = async (
  req,
  client_id = config.data.client_id,
  client_secret = config.data.client_secret,
  redirect_uri = config.data.redirectUri
) => {
  const oauthdata = {
    url: 'https://api.intra.42.fr/oauth/token',
    grant_type: config.data.grant_type,
    client_id: client_id,
    client_secret: client_secret,
    redirect_uri: redirect_uri,
    code: req.code,
    scope: 'public'
  }

  if (SINGLETON === 0) {
    await getToken(client, oauthdata)
    SINGLETON++
  }

  await interceptor(client, oauthdata)
  return client
}
